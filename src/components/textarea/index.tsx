import React, { FC, ChangeEvent } from 'react';
import classnames from 'classnames';

import './style.scss';

const Textarea: FC<{
  className: string;
  label: string;
  placeholder?: string;
  value: string;
  onChange: (value: string) => void;
}> = ({ className, label, placeholder = '', value, onChange }) => {
  return (
    <div className="textarea">
      <div className="form-group__label textarea__label">{label}</div>
      <textarea
        className={classnames('textarea__control', className)}
        placeholder={placeholder}
        value={value}
        onChange={(ev: ChangeEvent<HTMLTextAreaElement>) => onChange(ev.target.value)}
      />
    </div>
  );
};

export default Textarea;
