import React, { FC, MouseEvent } from 'react';
import classnames from 'classnames';

import { BUTTON_TYPES } from './types';

import './style.scss';

const Textarea: FC<{
  className?: string;
  type?: BUTTON_TYPES;
  disabled?: boolean;
  children: string;
  onClick: () => void;
}> = ({ className = '', type = BUTTON_TYPES.SECONDARY, disabled = false, children, onClick }) => {
  const handleClick = (ev: MouseEvent<HTMLButtonElement>) => {
    ev.preventDefault();

    onClick();
  };

  return (
    <button
      className={classnames(
        'btn-solid',
        {
          ['btn-solid--primary']: type === BUTTON_TYPES.PRIMARY,
          ['btn-solid--secondary']: type === BUTTON_TYPES.SECONDARY,
        },
        className
      )}
      onClick={ev => handleClick(ev)}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export default Textarea;
