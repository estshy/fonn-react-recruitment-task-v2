import React, { FC, ReactElement, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '~core/store';
import Modal from '~components/modal';
import Button from '~components/button';
import { BUTTON_TYPES } from '~components/button/types';

import {
  fetchMembers,
  resetState,
  restoreState,
  selectMembersWithSelectionState,
  toggleMemberInMultipleSelection,
  toggleMemberInSingleSelection,
} from './slice';
import { FetchingModes, Member, MembersModalModes } from './types';
import Table from './table';

import './style.scss';

const MembersModal: FC<{
  mode: MembersModalModes;
  initialSelection: Member[];
  minSelection?: number;
  onSubmit: (selectedMembers: Member[]) => void;
  onClose: () => void;
}> = ({
  mode,
  initialSelection = [],
  minSelection = mode === MembersModalModes.SINGLE_SELECTION ? 1 : 0,
  onSubmit,
  onClose,
}) => {
  const dispatch = useDispatch();
  const status = useSelector((state: RootState) => state.memberPicker.foregroundStatus);
  const members = useSelector(selectMembersWithSelectionState);
  const selectedMembers = useSelector((state: RootState) => state.memberPicker.selectedMembers);

  useEffect(() => {
    dispatch(
      restoreState({
        selectedMembers: initialSelection,
      })
    );
    dispatch(
      fetchMembers({
        startIndex: 0,
        stopIndex: 40,
        mode: FetchingModes.FOREGROUND,
      })
    );
  }, []);

  const toggleMember = (memberId: string) => {
    if (mode === MembersModalModes.SINGLE_SELECTION) {
      dispatch(
        toggleMemberInSingleSelection({
          memberId,
        })
      );
    }

    if (mode === MembersModalModes.MULTIPLE_SELECTION) {
      dispatch(
        toggleMemberInMultipleSelection({
          memberId,
        })
      );
    }
  };

  const handleClose = () => {
    dispatch(resetState());

    onClose();
  };

  const handleSubmit = () => {
    dispatch(resetState());

    onSubmit(selectedMembers);
  };

  let content: ReactElement | string = '';

  if (status === 'idle') {
    content = <Table members={members} onToggle={(memberId: string) => toggleMember(memberId)} />;
  }

  if (status === 'loading') {
    content = 'Loading...';
  }

  if (status === 'error') {
    content = 'Error has occurred';
  }

  const submitBtnText = useMemo(() => {
    if (mode === MembersModalModes.SINGLE_SELECTION) {
      return 'set respondent';
    }

    if (mode === MembersModalModes.MULTIPLE_SELECTION) {
      return 'save changes';
    }

    return '';
  }, [mode]);

  return (
    <Modal.Container className="qa-member-picker" onClose={() => onClose()}>
      <Modal.Header>Set respondent</Modal.Header>
      <Modal.Content>{content}</Modal.Content>
      <Modal.Footer>
        <div className="btn-group btn-group--form">
          <Button className="btn-group__item qa-rfi__cancel" onClick={() => handleClose}>
            cancel
          </Button>
          <Button
            className="btn-group__item qa-member-picker__submit"
            type={BUTTON_TYPES.PRIMARY}
            disabled={selectedMembers.length < minSelection}
            onClick={() => handleSubmit()}
          >
            {/* @TODO Discuss possibility of making a more generic button text or a need for customization per usage */}
            {submitBtnText}
          </Button>
        </div>
      </Modal.Footer>
    </Modal.Container>
  );
};

export default MembersModal;
