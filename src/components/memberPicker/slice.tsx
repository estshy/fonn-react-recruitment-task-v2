import { createSlice, createAsyncThunk, PayloadAction, AnyAction } from '@reduxjs/toolkit';
import { query } from '~core/api';
import { RootState } from '~core/store';
import {
  MemberPickerState,
  Member,
  MemberDTO,
  FetchAllMembersResponse,
  FetchAllMembersParams,
  FetchingModes,
} from './types';

const initialState: MemberPickerState = {
  members: [],
  selectedMembers: [],
  foregroundStatus: 'idle',
  backgroundStatus: 'idle',
  totalCount: 0,
};

export const fetchMembers = createAsyncThunk<FetchAllMembersResponse, FetchAllMembersParams>(
  'memberPicker/fetchMembers',
  function ({ startIndex, stopIndex }) {
    return query('members', {
      offset: `${startIndex}`,
      limit: `${stopIndex - startIndex + 1}`,
    });
  }
);

const slice = createSlice({
  name: 'membersPicker',
  initialState,
  reducers: {
    restoreState(state: MemberPickerState, action: PayloadAction<{ selectedMembers: Member[] }>) {
      state.selectedMembers = action.payload.selectedMembers;
    },

    resetState(state: MemberPickerState) {
      Object.assign(state, initialState);
    },

    toggleMemberInSingleSelection(state: MemberPickerState, action: PayloadAction<{ memberId: string }>) {
      const toggledMember = state.members.find(member => member && member.id === action.payload.memberId) as Member;

      if (state.selectedMembers.length > 0 && state.selectedMembers[0].id === toggledMember.id) {
        state.selectedMembers = [];
      } else {
        state.selectedMembers = [toggledMember];
      }
    },

    toggleMemberInMultipleSelection(state: MemberPickerState, action: PayloadAction<{ memberId: string }>) {
      const toggledMember = state.members.find(member => member && member.id === action.payload.memberId) as Member;
      const selectedMembersIds = state.selectedMembers.map(member => member.id);

      if (selectedMembersIds.indexOf(toggledMember.id) === -1) {
        state.selectedMembers.push(toggledMember);
      } else {
        state.selectedMembers = state.selectedMembers.filter(member => member.id !== toggledMember.id);
      }
    },
  },
  extraReducers: {
    [fetchMembers.pending.type]: (
      state: MemberPickerState,
      action: PayloadAction<void, any, { arg: { mode: FetchingModes } }>
    ) => {
      if (action.meta.arg.mode === FetchingModes.FOREGROUND) {
        state.foregroundStatus = 'loading';
      }

      if (action.meta.arg.mode === FetchingModes.BACKGROUND) {
        state.backgroundStatus = 'loading';
      }
    },
    [fetchMembers.rejected.type]: (
      state: MemberPickerState,
      action: PayloadAction<void, any, { arg: { mode: FetchingModes } }>
    ) => {
      if (action.meta.arg.mode === FetchingModes.FOREGROUND) {
        state.foregroundStatus = 'error';
      }

      if (action.meta.arg.mode === FetchingModes.BACKGROUND) {
        state.backgroundStatus = 'error';
      }
    },
    [fetchMembers.fulfilled.type]: (
      state: MemberPickerState,
      action: PayloadAction<FetchAllMembersResponse, any, { arg: FetchAllMembersParams }>
    ) => {
      for (let i = 0; i < action.payload.data.length; i++) {
        state.members[action.meta.arg.startIndex + i] = action.payload.data[i];
      }

      if (action.meta.arg.mode === FetchingModes.FOREGROUND) {
        state.foregroundStatus = 'idle';
      }

      if (action.meta.arg.mode === FetchingModes.BACKGROUND) {
        state.backgroundStatus = 'idle';
      }

      state.totalCount = action.payload.meta.pagination.totalCount;
    },
  },
});

export const selectMembersWithSelectionState = (state: RootState) => {
  const selectedMembersIds = state.memberPicker.selectedMembers.map((member: Member) => member.id);

  return state.memberPicker.members.map((member: Member) => {
    return {
      ...member,
      isSelected: selectedMembersIds.indexOf(member.id) !== -1,
    } as MemberDTO;
  });
};

export const { restoreState, resetState, toggleMemberInSingleSelection, toggleMemberInMultipleSelection } =
  slice.actions;

export default slice.reducer;
