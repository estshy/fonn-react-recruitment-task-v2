export interface MemberPickerState {
  members: Member[];
  selectedMembers: Member[];
  foregroundStatus: string;
  backgroundStatus: string;
  totalCount: number;
}

export interface Member {
  id: string;
  firstName: string;
  lastName: string;
  company: string;
  email: string;
  role: string;
}

export interface MemberDTO extends Member {
  isSelected: boolean;
}

export interface FetchAllMembersResponse {
  meta: {
    pagination: {
      totalCount: number;
      maxPages: number;
      onPage: number;
      page: number;
      nextPage: number | null;
      previousPage: number | null;
    };
  };
  data: Member[];
}

export interface FetchAllMembersParams {
  startIndex: number;
  stopIndex: number;
  mode: FetchingModes;
}

export enum MembersModalModes {
  SINGLE_SELECTION,
  MULTIPLE_SELECTION,
}

export enum FetchingModes {
  FOREGROUND,
  BACKGROUND
}