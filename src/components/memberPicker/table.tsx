import React, { FC, useLayoutEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classnames from 'classnames';
import { debounce } from 'throttle-debounce';
import { List, InfiniteLoader } from 'react-virtualized';

import { RootState } from '~core/store';
import { fetchMembers } from './slice';
import { FetchingModes, MemberDTO } from './types';

const SCREEN_RESIZE_DEBOUNCE_TIME = 250;

const MembersModal: FC<{
  members: MemberDTO[];
  onToggle: (memberId: string) => void;
}> = ({ members, onToggle }) => {
  const dispatch = useDispatch();
  const scrollableContainer = useRef<HTMLDivElement>(null);
  const totalCount = useSelector((state: RootState) => state.memberPicker.totalCount);
  const [listWidth, setListWidth] = useState<null | number>(null);
  const [listHeight, setListHeight] = useState<null | number>(null);

  useLayoutEffect(() => {
    const calculateViewBox = () => {
      setListWidth(scrollableContainer.current!.getClientRects()[0].width);
      setListHeight(scrollableContainer.current!.getClientRects()[0].height);
    };
    const calculateViewBoxDebounced = debounce(SCREEN_RESIZE_DEBOUNCE_TIME, calculateViewBox);

    calculateViewBox();

    window.addEventListener('resize', calculateViewBoxDebounced);

    return () => {
      window.removeEventListener('resize', calculateViewBoxDebounced);
    };
  }, []);

  const formatRole = (role: string) => {
    switch (role) {
      case 'supervisor':
        return 'Supervisor';
      case 'project-manager':
        return 'Project manager';
      case 'contributor':
        return 'Contributor';
      case 'subcontractor':
        return 'Subcontractor';
      default:
        return role;
    }
  };

  const loadMoreRows = ({ startIndex, stopIndex }: { startIndex: number; stopIndex: number }) => {
    return dispatch(
      fetchMembers({
        mode: FetchingModes.BACKGROUND,
        startIndex: startIndex,
        stopIndex: stopIndex,
      })
    );
  };

  const rowRenderer = ({ key, index, style }: { key: string; index: number; style: object }) => {
    const member = members[index];

    if (typeof member === 'undefined') {
      return (
        <div key={key} style={style} className="members-list__row">
          <div className="members-list__td members-list__td--photo" />
          <div className="members-list__td members-list__td--name">
            <div className="members-list__name" />
            <div className="members-list__email" />
          </div>
          <div className="members-list__td members-list__td--role" />
          <div className="members-list__td members-list__td--company" />
          <div className="members-list__td members-list__td--action" />
        </div>
      );
    }

    return (
      <div
        key={key}
        style={style}
        className={classnames('members-list__row qa-member', {
          'members-list__row--selected': member.isSelected,
        })}
      >
        <div className="members-list__cell members-list__cell--photo">
          <img src={`https://i.pravatar.cc/150?u=${member.id}`} className="members-list__photo" />
        </div>
        <div className="members-list__cell members-list__cell--name">
          <div className="members-list__name">
            {member.firstName} {member.lastName}
          </div>
          <div className="members-list__email">{member.email}</div>
        </div>
        <div className="members-list__cell members-list__cell--role">{formatRole(member.role)}</div>
        <div className="members-list__cell members-list__cell--company">{member.company}</div>
        <div className="members-list__cell members-list__cell--action">
          <input type="checkbox" checked={member.isSelected} onChange={() => onToggle(member.id)} />
        </div>
      </div>
    );
  };

  return (
    <>
      <div className="members-list__header">
        <div className="members-list__cell members-list__cell--photo" />
        <div className="members-list__cell members-list__cell--name">Name</div>
        <div className="members-list__cell members-list__cell--role">Role</div>
        <div className="members-list__cell members-list__cell--company">Company</div>
        <div className="members-list__cell members-list__cell--action" />
      </div>
      <div className="members-list" ref={scrollableContainer}>
        {listWidth && listHeight ? (
          <InfiniteLoader
            isRowLoaded={index => typeof members[index] !== 'undefined'}
            loadMoreRows={loadMoreRows}
            minimumBatchSize={10}
            rowCount={totalCount}
            threshold={10}
          >
            {({ onRowsRendered, registerChild }) => (
              <List
                width={listWidth!}
                height={listHeight!}
                rowCount={totalCount}
                rowHeight={70} //@todo should be derived automatically based on body font-size, it should match 4em
                ref={registerChild}
                onRowsRendered={onRowsRendered}
                rowRenderer={rowRenderer}
              />
            )}
          </InfiniteLoader>
        ) : (
          ''
        )}
      </div>
    </>
  );
};

export default MembersModal;
