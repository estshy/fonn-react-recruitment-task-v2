import React, { FC, ReactElement, createContext, useContext } from 'react';
import { createPortal } from 'react-dom';
import classnames from 'classnames';

import { TimesIcon } from '~assets/icons';

import './style.scss';

const ModalContext = createContext<{
  onClose: () => void;
}>({
  onClose: () => {},
});

const Container: FC<{
  className?: string;
  children: ReactElement | ReactElement[] | string; //@todo check for validating against specific types of components
  onClose: () => void;
}> = ({ className = '', children, onClose }) => {
  return createPortal(
    <div className="modal__container">
      <div className={classnames('modal', className)}>
        <ModalContext.Provider
          value={{
            onClose,
          }}
        >
          {children}
        </ModalContext.Provider>
      </div>
    </div>,
    document.getElementById('outlet')!
  );
};

const Header: FC<{
  children: string;
}> = ({ children }) => {
  const { onClose } = useContext(ModalContext);

  return (
    <div className="modal__header">
      <div className="modal__title">{children}</div>
      <div className="modal__close" onClick={() => onClose()}>
        <TimesIcon />
      </div>
    </div>
  );
};

const Content: FC<{
  children: ReactElement | ReactElement[] | string;
}> = ({ children }) => {
  return <div className="modal__content">{children}</div>;
};

const Footer: FC<{
  children: ReactElement | ReactElement[] | string;
}> = ({ children }) => {
  return <div className="modal__footer">{children}</div>;
};

export default {
  Container,
  Header,
  Content,
  Footer,
};
