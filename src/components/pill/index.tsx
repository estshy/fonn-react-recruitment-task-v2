import React, { FC } from 'react';
import classnames from 'classnames';

import { TimesIcon } from '~assets/icons';

import './style.scss';

const Pill: FC<{
  className?: string;
  children: string;
  onRemove: () => void;
}> = ({ className = '', children, onRemove }) => {
  return (
    <div className={classnames('pill', className)}>
      <div className="pill__text">{children}</div>

      <div className="pill__remove qa-pill--remove" onClick={() => onRemove()}>
        <TimesIcon />
      </div>
    </div>
  );
};

export default Pill;
