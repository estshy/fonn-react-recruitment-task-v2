import React, { FC, useEffect, useState, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '~core/store';
import MembersModal from '~components/memberPicker';
import { Member, MembersModalModes } from '~components/memberPicker/types';
import {
  resetForm,
  fetchDefaultMembers,
  changeSelectedRespondent,
  changeSelectedFollowers,
} from './slice';

import Form from './form';

import './style.scss';

const Index: FC = () => {
  const dispatch = useDispatch();

  const status: string = useSelector((state: RootState) => state.rfi.status);
  const question: string = useSelector((state: RootState) => state.rfi.values.question);
  const selectedRespondent: Member | null = useSelector((state: RootState) => state.rfi.values.respondent);
  const selectedFollowers: Member[] = useSelector((state: RootState) => state.rfi.values.followers);

  const [isSelectingRespondent, changeIsSelectingRespondent] = useState(false);
  const [isSelectingFollowers, changeIsSelectingFollowers] = useState(false);

  const isValid = useMemo(() => {
    if (question.length === 0) {
      return false;
    }

    return selectedRespondent !== null;
  }, [question, selectedRespondent, selectedFollowers]);

  const handleRespondentChosen = (member: Member) => {
    dispatch(
      changeSelectedRespondent({
        member,
      })
    );

    changeIsSelectingRespondent(false);
  };

  const handleFollowersChosen = (members: Member[]) => {
    dispatch(
      changeSelectedFollowers({
        members,
      })
    );

    changeIsSelectingFollowers(false);
  };

  const onSubmit = () => {
    if (!isValid) {
      return;
    }

    dispatch(resetForm());
  };

  useEffect(() => {
    if (status === 'idle') {
      dispatch(fetchDefaultMembers());
    }
  }, []);

  let form;

  if (status === 'idle') {
    form = (
      <Form
        isValid={isValid}
        onSelectingRespondent={() => changeIsSelectingRespondent(true)}
        onSelectingFollowers={() => changeIsSelectingFollowers(true)}
        onSubmit={() => onSubmit()}
      />
    );
  }

  if (status === 'loading') {
    form = <div>Loading...</div>;
  }

  if (status === 'error') {
    form = <div>Error has occurred</div>;
  }

  return (
    <div className="rfi">
      <div className="rfi__header">
        <h1 className="headline--primary">New request for information</h1>
      </div>

      {form}

      {isSelectingRespondent ? (
        <MembersModal
          mode={MembersModalModes.SINGLE_SELECTION}
          initialSelection={selectedRespondent ? [selectedRespondent] : []}
          onSubmit={(members: Member[]) => handleRespondentChosen(members[0])}
          onClose={() => changeIsSelectingRespondent(false)}
        />
      ) : (
        ''
      )}

      {isSelectingFollowers ? (
        <MembersModal
          mode={MembersModalModes.MULTIPLE_SELECTION}
          initialSelection={selectedFollowers}
          onSubmit={(members: Member[]) => handleFollowersChosen(members)}
          onClose={() => changeIsSelectingFollowers(false)}
        />
      ) : (
        ''
      )}
    </div>
  );
};

export default Index;
