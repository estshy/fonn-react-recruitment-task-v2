import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';

import { query } from '~core/api';
import { Member } from '~components/memberPicker/types';
import { FormState } from './types';

const initialState: FormState = {
  values: {
    question: '',
    respondent: null,
    followers: [],
  },
  defaultMembers: [],
  status: 'idle',
};

export const fetchDefaultMembers = createAsyncThunk('rfi/defaultMembers', function () {
  return query('members/default');
});

const slice = createSlice({
  name: 'rfi',
  initialState,
  reducers: {
    changeQuestion(state: FormState, action: PayloadAction<{ question: string }>) {
      state.values.question = action.payload.question;
    },

    removeSelectedRespondent(state: FormState) {
      state.values.respondent = null;
    },

    removeSelectedFollower(state: FormState, action: PayloadAction<{ followerId: string }>) {
      state.values.followers = state.values.followers.filter(follower => follower.id !== action.payload.followerId);
    },

    changeSelectedRespondent(state: FormState, action: PayloadAction<{ member: Member }>) {
      state.values.respondent = action.payload.member;
    },

    changeSelectedFollowers(state: FormState, action: PayloadAction<{ members: Member[] }>) {
      state.values.followers = action.payload.members;
    },

    resetForm: (state: FormState) => {
      state.values = {
        ...initialState.values,
        followers: [...state.defaultMembers],
      };
    },
  },
  extraReducers: {
    [fetchDefaultMembers.pending.type]: (state: FormState) => {
      state.status = 'loading';
    },
    [fetchDefaultMembers.rejected.type]: (state: FormState) => {
      state.status = 'error';
    },
    [fetchDefaultMembers.fulfilled.type]: (state: FormState, action: PayloadAction<{ data: Member[] }>) => {
      state.values.followers = action.payload.data;
      state.defaultMembers = action.payload.data;
      state.status = 'idle';
    },
  },
});

export const {
  changeQuestion,
  removeSelectedRespondent,
  removeSelectedFollower,
  changeSelectedRespondent,
  changeSelectedFollowers,
  resetForm,
} = slice.actions;

export default slice.reducer;
