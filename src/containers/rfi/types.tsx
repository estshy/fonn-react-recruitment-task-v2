import { Member } from "~components/memberPicker/types";

export interface FormState {
  values: {
    question: string;
    respondent: Member | null;
    followers: Member[];
  };
  defaultMembers: Member[];
  status: string;
}