import React, { FC } from 'react';
import classnames from 'classnames';

import { Member } from '~components/memberPicker/types';
import Button from '~components/button';
import { TimesIcon } from '~assets/icons';

import './style.scss';

const Respondent: FC<{
  className?: string;
  respondent: Member;
  onChange: () => void;
  onRemove: () => void;
}> = ({ className = '', respondent, onChange, onRemove }) => {
  return (
    <div className={classnames('respondent qa-respondent', className)}>
      <img src={`https://i.pravatar.cc/150?u=${respondent.id}`} className="respondent__photo" />

      <div className="respondent__details">
        <div className="respondent__name qa-respondent__name">
          {respondent.firstName} {respondent.lastName}
        </div>
        <div className="respondent__email">
          {respondent.email}
        </div>
      </div>
      <div className="respondent__change">
        <Button className="qa-respondent--change" onClick={() => onChange()}>
          change
        </Button>
      </div>
      <div className="respondent__remove qa-respondent--remove" onClick={() => onRemove()}>
        <TimesIcon />
      </div>
    </div>
  );
};

export default Respondent;
