import React, { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '~core/store';
import { Member } from '~components/memberPicker/types';
import Textarea from '~components/textarea';
import Button from '~components/button';
import Pill from '~components/pill';

import { BUTTON_TYPES } from '~components/button/types';

import Respondent from './respondent';
import { changeQuestion, removeSelectedFollower, removeSelectedRespondent, resetForm } from './slice';

const Index: FC<{
  isValid: boolean;
  onSelectingRespondent: () => void;
  onSelectingFollowers: () => void;
  onSubmit: () => void;
}> = ({ isValid, onSelectingRespondent, onSelectingFollowers, onSubmit }) => {
  const dispatch = useDispatch();

  const question = useSelector((state: RootState) => state.rfi.values.question);
  const selectedRespondent = useSelector((state: RootState) => state.rfi.values.respondent);
  const selectedFollowers = useSelector((state: RootState) => state.rfi.values.followers);

  return (
    <form>
      <div className="form-group">
        <div className="form-group__description">
          <h3 className="form-group__name">Request</h3>
          <div className="form-group__text">Provide a detailed description of the issue you want to clarify</div>
        </div>
        <div className="form-group__control">
          <Textarea
            className="qa-rfi__question"
            label="Question"
            placeholder="Type question here..."
            value={question}
            onChange={value =>
              dispatch(
                changeQuestion({
                  question: value,
                })
              )
            }
          />
        </div>
      </div>
      <div className="form-group">
        <div className="form-group__description">
          <h3 className="form-group__name">Discussion</h3>
          <div className="form-group__text">Select people you think are necessary to collaborate on the question</div>
        </div>
        <div className="form-group__control">
          <div className="form-group__section">
            <div className="form-group__label">Respondent</div>

            <div className="form-group__text">
              Designate a person who will be responsible for giving the official reply
            </div>

            {selectedRespondent ? (
              <Respondent
                className="rfi__respondent qa-rfi__selected-respondent"
                respondent={selectedRespondent}
                onChange={() => onSelectingRespondent()}
                onRemove={() => dispatch(removeSelectedRespondent())}
              />
            ) : (
              <Button className="rfi__btn qa-rfi__select-respondent" onClick={() => onSelectingRespondent()}>
                set respondent
              </Button>
            )}
          </div>

          <div className="form-group__section">
            <div className="form-group__label">
              Followers <span className="form-group__remark">(optional)</span>
            </div>

            {selectedFollowers.length > 0 ? (
              <div className="rfi__followers">
                {selectedFollowers.map((follower: Member) => (
                  <Pill
                    key={follower.id}
                    className="rfi__follower qa-rfi__selected-follower"
                    onRemove={() =>
                      dispatch(
                        removeSelectedFollower({
                          followerId: follower.id,
                        })
                      )
                    }
                  >
                    {`${follower.firstName} ${follower.lastName}`}
                  </Pill>
                ))}
              </div>
            ) : (
              ''
            )}

            <div>
              <Button className="rfi__btn qa-rfi__selected-followers" onClick={() => onSelectingFollowers()}>
                change followers
              </Button>
            </div>
          </div>
        </div>
      </div>
      <div className="btn-group btn-group--form">
        <Button className="btn-group__item qa-rfi__cancel" onClick={() => dispatch(resetForm())}>
          Cancel
        </Button>
        <Button
          className="btn-group__item qa-rfi__submit"
          type={BUTTON_TYPES.PRIMARY}
          disabled={!isValid}
          onClick={() => onSubmit()}
        >
          Create RFI
        </Button>
      </div>
    </form>
  );
};

export default Index;
