import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Rfi from '~containers/rfi';
import store from '~core/store';

import './styles/index.scss';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <div className="container">
        <Rfi />
      </div>
    </Provider>
  </React.StrictMode>,
  document.getElementById('app')
);
