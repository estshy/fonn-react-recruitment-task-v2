import 'whatwg-fetch';

function getFullUrl(path: string, queryParams?: Record<string, string>) {
  const apiUrl = process.env.REACT_API_URL;

  if (typeof queryParams !== 'undefined') {
    return `${apiUrl}${path}` + `?` + new URLSearchParams(queryParams);
  }

  return `${apiUrl}${path}`;
}

export const query = (path: string, queryParams?: Record<string, string>) => {
  const fullUrl = getFullUrl(path, queryParams);

  return fetch(fullUrl).then(response => response.json());
};
