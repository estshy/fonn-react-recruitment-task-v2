import { configureStore } from "@reduxjs/toolkit";

import RfiReducer from '~containers/rfi/slice';
import MemberPickerReducer from '~components/memberPicker/slice';

const store = configureStore({
  reducer: {
    rfi: RfiReducer,
    memberPicker: MemberPickerReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;