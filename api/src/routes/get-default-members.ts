import {Envelope} from "../common/envelope";
import {Member} from "../types/member";
import {defaultMembers} from "../mock-data/default-members";

export const getDefaultMembers = (req, res, next) => {
  const response = new Envelope<Member[]>(defaultMembers, {});

  res.status(200).json(response);
  next();
}
