import {PORT} from "./config";
import {Role} from "./types/role";

export const Swagger = {
  "openapi": "3.0.1",
  "info": {
    "title": "Fonn React Recruitment Task",
    "description": "This document describes Mock API specification",
    "version": "1.0.0"
  },
  "servers": [
    {
      "url": `http://localhost:${PORT}`
    }
  ],
  "paths": {
    "/members": {
      "get": {
        "tags": [
          "members"
        ],
        "summary": "Get members list",
        "operationId": "getMembers",
        "parameters": [
          {
            "name": "page",
            "in": "query",
            "required": true,
            "schema": {
              "type": "number"
            }
          },
          {
            "name": "onPage",
            "in": "query",
            "required": true,
            "schema": {
              "type": "number"
            }
          },
          {
            "name": "search",
            "in": "query",
            "required": false,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "role",
            "in": "query",
            "required": false,
            "schema": {
              "$ref": "#/components/schemas/Role"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Members list",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "meta": {
                      "$ref": "#/components/schemas/MetaPagination"
                    },
                    "data": {
                      "type": "array",
                      "items": {
                        "$ref": "#/components/schemas/Member"
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Invalid id or body passed",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/BadRequestError"
                }
              }
            }
          },
          "500": {
            "description": "Internal exception",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UnexpectedError"
                }
              }
            }
          }
        }
      }
    },
    "/members/default": {
      "get": {
        "tags": [
          "members"
        ],
        "summary": "Get default selected members list",
        "operationId": "getDefaultSelectedMembers",
        "responses": {
          "200": {
            "description": "Resource members list",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "meta": {
                      "$ref": "#/components/schemas/Meta"
                    },
                    "data": {
                      "type": "array",
                      "items": {
                        "$ref": "#/components/schemas/Member"
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Invalid id or body passed",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/BadRequestError"
                }
              }
            }
          },
          "500": {
            "description": "Internal exception",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/UnexpectedError"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "BadRequestError": {
        "type": "object",
        "required": [
          "code",
          "message"
        ],
        "properties": {
          "code": {
            "type": "string",
            "example": "error_code"
          },
          "message": {
            "type": "string",
            "example": "Bad request error message"
          }
        }
      },
      "UnexpectedError": {
        "type": "object",
        "required": [
          "code",
          "message"
        ],
        "properties": {
          "code": {
            "type": "string",
            "example": "unexpected_error"
          },
          "message": {
            "type": "string",
            "example": "Unexpected error"
          }
        }
      },
      "Pagination": {
        "type": "object",
        "required": [
          "totalCount",
          "maxPages",
          "onPage",
          "page"
        ],
        "properties": {
          "totalCount": {
            "type": "number",
            "format": "int",
            "example": 1000
          },
          "maxPages": {
            "type": "number",
            "format": "int",
            "example": 100
          },
          "onPage": {
            "type": "number",
            "format": "int",
            "example": 10
          },
          "page": {
            "type": "number",
            "format": "int",
            "example": 1
          },
          "nextPage": {
            "type": "number",
            "format": "int",
            "example": 2
          },
          "previousPage": {
            "type": "number",
            "format": "int",
            "example": null
          }
        }
      },
      "Meta": {
        "type": "object",
        "properties": {}
      },
      "MetaPagination": {
        "allOf": [
          {
            "$ref": "#/components/schemas/Meta"
          },
          {
            "type": "object",
            "properties": {
              "pagination": {
                "$ref": "#/components/schemas/Pagination"
              }
            }
          }
        ]
      },
      "Member": {
        "type": "object",
        "required": [
          "id",
          "email"
        ],
        "properties": {
          "id": {
            "type": "string",
            "format": "uuid"
          },
          "email": {
            "type": "string",
            "format": "email"
          },
          "firstName": {
            "type": "string",
            "example": "John"
          },
          "lastName": {
            "type": "string",
            "example": "Doe"
          },
          "company": {
            "type": "string",
            "example": "Fonn"
          }
        }
      },
      "Role": {
        "type": "string",
        "enum": Object.values(Role),
      }
    }
  }
}
