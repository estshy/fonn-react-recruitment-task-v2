export class Envelope<TData, TMeta = {}> {
  public readonly meta: TMeta;
  public readonly data: TData;

  constructor(data: TData, meta: TMeta) {
    this.meta = meta;
    this.data = data;

    Object.freeze(this);
  }
}
