import {Pagination} from "./pagination";
import {Request} from "express";
import {BadRequest} from "./errors/bad-request";

export class PaginationRequestFactory {

  private constructor() {
  }

  static buildFromRequest(req: Request, totalCount: number): Pagination {
    const {page, onPage} = req.query;

    if (!page || (+page < 1 && +page >= Infinity)) {
      throw new BadRequest('page_query', 'Wrong page query parameter');
    } else if (!onPage || (+onPage < 1 && +onPage >= 50)) {
      throw new BadRequest('on_page_query', 'Wrong onPage parameter');
    }

    return new Pagination(+page, +onPage, totalCount);
  }
}
