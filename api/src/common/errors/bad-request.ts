import {HttpError} from "./http-error";

export class BadRequest extends HttpError {
  constructor(errorCode: string, message: string) {
    super(400, errorCode, message);
  }
}
