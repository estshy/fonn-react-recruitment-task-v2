export class HttpError extends Error {
  constructor(
    public httpCode: number,
    public errorCode: string,
    public message: string
  ) {
    super(message);
  }
}
