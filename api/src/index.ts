import * as cors from "cors";
import * as express from "express";
import * as swaggerUi from "swagger-ui-express";
import {Swagger} from "./swagger";
import {HttpError} from "./common/errors/http-error";
import {getMembers} from "./routes/get-members";
import {getDefaultMembers} from "./routes/get-default-members";
import {PORT} from "./config";

const api = express();
api.use(cors());
api.use(express.json());

api.use('/docs', swaggerUi.serve, swaggerUi.setup(Swagger));
api.get('/members', getMembers);
api.get('/members/default', getDefaultMembers);

api.use((err, req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }

  if (err instanceof HttpError) {
    res.status(err.httpCode).json({
      code: err.errorCode,
      message: err.message
    })
  } else {
    res.status(500).json({
      code: 'unexpected_error',
      message: 'Unexpected error occurred!'
    })
  }

  next(err);
})

api.listen(PORT, () => {
  console.log(`Mock api is running...`);
  console.log(`  API Url: http://localhost:${PORT}`);
  console.log(`  Swagger: http://localhost:${PORT}/docs`);
});
