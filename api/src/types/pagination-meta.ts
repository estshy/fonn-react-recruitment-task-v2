import {Pagination} from "../common/pagination";

export type PaginationMeta = { pagination: Pagination }
