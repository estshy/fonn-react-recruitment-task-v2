import {RoleType} from "./role";

export type Member = {
  id: string,
  email: string,
  firstName: string | null,
  lastName: string | null,
  company: string | null,
  role: RoleType
}
