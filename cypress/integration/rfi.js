describe('Request for information', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080');
    cy.wait(500); //@todo mock API instead to make sure it is deterministic
  });

  describe('As a User, I want to fill the RFI form to ask the respondent for a business question', () => {
    it('The "Create RFI" button should be disabled by default', () => {
      cy.get('.qa-rfi__submit').should('be.disabled');
    });

    it('The form should allow filling all required fields. The "Create RFI" button should be enabled when the question field is not empty and respondent is selected', () => {
      cy.get('.qa-rfi__question').type('How are we going to do it?');

      cy.get('.qa-rfi__select-respondent').click();
      cy.get(`.qa-member-picker .qa-member:nth-child(1) input[type="checkbox"]`).click({
        force: true
      });
      cy.get('.qa-member-picker__submit').click();

      cy.get('.qa-rfi__submit').should('not.be.disabled');
    });

    it('Default followers should be populated from API GET /members/default endpoint on the init of the form', () => {
      cy.get('.qa-rfi__selected-follower').should('have.length.at.least', 1);
    });
  });

  describe('As a User, I want to select a single member as an RFI Respondent to be assigned to the RFI', () => {
    it('There should be a possibility to fire member-picker in single-select mode', () => {
      cy.get('.qa-rfi__select-respondent').click();
      cy.get(`.qa-member-picker .qa-member:nth-child(1) input[type="checkbox"]`).click({
        force: true
      });
      cy.get(`.qa-member-picker .qa-member:nth-child(1) input[type="checkbox"]`).should('be.checked');
      cy.get(`.qa-member-picker .qa-member:nth-child(2) input[type="checkbox"]`).click({
        force: true
      });
      cy.get(`.qa-member-picker .qa-member:nth-child(1) input[type="checkbox"]`).should('not.be.checked');
      cy.get(`.qa-member-picker .qa-member:nth-child(2) input[type="checkbox"]`).should('be.checked');
      cy.get('.qa-member-picker__submit').click();
    });

    it('If there is already picked member, there should be a possibility to change him to another one', () => {
      let valueBefore;
      let valueAfter;

      cy.get('.qa-rfi__select-respondent').click();
      cy.get(`.qa-member-picker .qa-member:nth-child(1) input[type="checkbox"]`).click({
        force: true
      });
      cy.get('.qa-member-picker__submit').click();
      cy.get('.qa-rfi__selected-respondent .qa-respondent__name').then($el => {
        valueBefore = $el.text();
      });

      cy.get('.qa-rfi__selected-respondent .qa-respondent--change').click();
      cy.get(`.qa-member-picker .qa-member:nth-child(2) input[type="checkbox"]`).click({
        force: true
      });
      cy.get('.qa-member-picker__submit').click();
      cy.get('.qa-rfi__selected-respondent .qa-respondent__name').then($el => {
        valueAfter = $el.text();

        expect(valueBefore).to.not.equal(valueAfter);
      });
    });

    it('There should be a possibility to remove the selected member', () => {
      cy.get('.qa-rfi__select-respondent').click();
      cy.get(`.qa-member-picker .qa-member:nth-child(1) input[type="checkbox"]`).click({
        force: true
      });
      cy.get('.qa-member-picker__submit').click();
      cy.get('.qa-rfi__selected-respondent .qa-respondent--remove').click();
      cy.get('.qa-rfi__selected-respondent').should('not.exist');
    });

    it('The “Create RFI” button should be disabled if no member is picked', () => {
      cy.get('.qa-rfi__select-respondent').click();
      cy.get(`.qa-member-picker .qa-member:nth-child(1) input[type="checkbox"]`).click({
        force: true
      });
      cy.get('.qa-member-picker__submit').click();
      cy.get('.qa-rfi__selected-respondent .qa-respondent--remove').click();
      cy.get('.qa-rfi__selected-respondent').should('not.exist');
      cy.get('.qa-rfi__submit').should('be.disabled');
    });
  });

  describe('As a User, I want to select multiple members as the RFI Followers to notify interested people about the question', () => {
    it('There should be a possibility to fire member-picker in multi-select mode', () => {
      cy.get('.qa-rfi__selected-followers').click();
      //@todo Mock API and set default list of followers to be empty before this test
      cy.get('.qa-member input[type="checkbox"]:checked').each(($el) => {
        $el.click();
      });
      cy.get(`.qa-member-picker .qa-member:nth-child(1) input[type="checkbox"]`).click({
        force: true
      });
      cy.get(`.qa-member-picker .qa-member:nth-child(2) input[type="checkbox"]`).click({
        force: true
      });
      cy.get(`.qa-member-picker .qa-member:nth-child(1) input[type="checkbox"]`).should('be.checked');
      cy.get(`.qa-member-picker .qa-member:nth-child(2) input[type="checkbox"]`).should('be.checked');
      cy.get('.qa-member-picker__submit').click();
    });

    it('Available to select members should be populated from API GET /members endpoint', () => {
      //@todo mock API and then expect right endpoint to be called
    });

    it('There should be a possibility to remove each follower', () => {
      let followersBefore;
      let followersAfter;

      cy.get('.qa-rfi__selected-follower').then($elements => {
        followersBefore = $elements.length;
      });
      cy.get('.qa-rfi__selected-follower:first-child .qa-pill--remove').click();
      cy.get('.qa-rfi__selected-follower').then($elements => {
        followersAfter = $elements.length;

        expect(followersBefore).to.not.equal(followersAfter);
      });
    });

    it('Chosen followers can be removed directly in the followers\' picker modal.', () => {
      cy.get('.qa-rfi__selected-followers').click();
      cy.get('.qa-member input[type="checkbox"]:checked').each(($el) => {
        $el.click();
      });
      cy.get('.qa-member-picker__submit').click();
      cy.get('.qa-rfi__selected-follower').should('have.length', 0);
    });

    it('Already selected followers should be showed in member-picker', () => {
      cy.get('.qa-rfi__selected-followers').click();
      cy.get('.qa-member input[type="checkbox"]:checked').should('have.length.at.least', 1);
    });
  });
})