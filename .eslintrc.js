module.exports = {
  root: true,
  parser: '@typescript-eslint/parser', 
  plugins: ['@typescript-eslint', 'react', 'compat', 'prettier'],
  extends: [
    'plugin:react/recommended', 
    'plugin:prettier/recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint', 
  ],
  env: {
    browser: true,
    jest: true,
  },
  parserOptions: {
    ecmaVersion: 2018, 
    sourceType: 'module', 
    ecmaFeatures: {
      jsx: true, 
    },
  },
  settings: {
    react: {
      version: 'detect', 
    },
    
    polyfills: [
      'IntersectionObserver',
      'Promise',
      'Object',
      'URLSearchParams',
      'Array',
    ],
  },
  rules: {
    'prettier/prettier': 'warn',
    '@typescript-eslint/no-var-requires': 0,
    'compat/compat': 'warn',
    'react/prop-types': 'off',
    'react/no-children-prop': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/no-inferrable-types': 'off',
    '@typescript-eslint/no-empty-interface': 'off',
    '@typescript-eslint/no-use-before-define': 'off',
    '@typescript-eslint/no-empty-function': 'off',
    '@typescript-eslint/camelcase': 'off',
    '@typescript-eslint/no-unused-vars': ['warn', { ignoreRestSiblings: true }],
    '@typescript-eslint/ban-ts-ignore': 'off',
    'prefer-const': 'warn',
  },
};
