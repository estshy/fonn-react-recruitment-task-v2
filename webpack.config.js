const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const postcssPresetEnv = require('postcss-preset-env');
const postcssNormalize = require('postcss-normalize');
const { EnvironmentPlugin } = require('webpack');

require('dotenv').config();

module.exports = {
  entry: './src/index.tsx',
  output: {
    path: path.join(__dirname, 'bundle'),
    filename: 'index_bundle.js',
    publicPath: '',
  },
  devServer: {
    inline: true,
    port: 8080,
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    alias: {
      '~components': path.resolve(__dirname, path.join('src', 'components')),
      '~containers': path.resolve(__dirname, path.join('src', 'containers')),
      '~core': path.resolve(__dirname, path.join('src', 'core')),
      '~styles': path.resolve(__dirname, path.join('src', 'styles')),
      '~assets': path.resolve(__dirname, path.join('src', 'assets')),
    },
  },
  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              indent: 'postcss',
              plugins: () => [postcssPresetEnv(), postcssNormalize()],
            },
          },
          'sass-loader',
        ],
      },
      { test: /\.(ts|js)x?$/, loader: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.(js|jsx|ts|tsx)$/,
        enforce: 'pre',
        use: [
          {
            options: {
              cache: true,
              eslintPath: require.resolve('eslint'),
              resolvePluginsRelativeTo: __dirname,
            },
            loader: 'eslint-loader',
          },
        ],
        include: path.resolve(__dirname, '../src'),
      },
      { test: /\.(ts|js)x?$/, loader: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.woff/,
        type: 'asset/resource',
      },
      {
        test: /\.svg/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
      chunkFilename: '[id].[contenthash].css',
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
    new EnvironmentPlugin(['REACT_API_URL']),
  ],
};
